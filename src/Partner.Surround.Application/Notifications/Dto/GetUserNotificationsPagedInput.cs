﻿using Abp.Notifications;
using Partner.Surround.CommonDto;

namespace Partner.Surround.Notifications.Dto
{
    public class GetUserNotificationsPagedInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}
