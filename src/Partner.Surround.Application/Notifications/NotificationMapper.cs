﻿using Abp.Notifications;
using AutoMapper;
using Partner.Surround.Notifications.Dto;

namespace Partner.Surround.Notifications
{
    internal static class NotificationMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
        }
    }
}
