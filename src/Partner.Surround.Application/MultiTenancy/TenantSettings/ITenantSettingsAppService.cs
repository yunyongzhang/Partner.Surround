﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Partner.Surround.MultiTenancy.TenantSetting.Dto;

namespace Partner.Surround.MultiTenancy.TenantSetting
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);
    }
}
