namespace Partner.Surround.MultiTenancy.TenantSetting.Dto
{
    public class TenantOtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}