﻿namespace Partner.Surround.Host.HostSettings.Dto
{
    public class HostOtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}