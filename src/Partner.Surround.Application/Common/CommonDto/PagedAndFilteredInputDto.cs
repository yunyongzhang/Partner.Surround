﻿using Abp.Application.Services.Dto;

namespace Partner.Surround.CommonDto
{
    /// <summary>
    /// 分页及筛选Dto
    /// </summary>
    public class PagedAndFilteredInputDto : PagedInputDto, IPagedResultRequest
    {
        public PagedAndFilteredInputDto()
        {
            MaxResultCount = SurroundApplicationConsts.DefaultPageSize;
        }

        public string FilterText { get; set; }
    }
}
