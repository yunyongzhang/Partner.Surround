﻿using Abp.Auditing;
using AutoMapper;
using Partner.Surround.Auditing.Dto;

namespace Partner.Surround.Monitoring
{
    internal static class MonitoringMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<AuditLog, AuditLogListDto>();
        }
    }
}
