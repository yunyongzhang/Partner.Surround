﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Partner.Surround.Authorization;
using Partner.Surround.Monitoring;
using Partner.Surround.MultiTenancy;
using Partner.Surround.Notifications;
using Partner.Surround.Organizations;
using Partner.Surround.Resource;
using Partner.Surround.Social;
using Partner.Surround.Storage.Minio;
using Partner.Surround.TaskCenter;

namespace Partner.Surround
{
    [DependsOn(
        typeof(SurroundCoreModule),
        typeof(SurroundMinioStorageModule),
        typeof(AbpAutoMapperModule))]
    public class SurroundApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            // AutoMapper
            Configuration.Modules.AbpAutoMapper().Configurators.Add(AuthorizationMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(OrganizationMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(SocialMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(NotificationMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(MonitoringMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(MultiTenancyMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(ResourceMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(TaskCenterMapper.CreateMappings);
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(SurroundApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }

        public override void PostInitialize()
        {
            base.PostInitialize();
        }

        public override void Shutdown()
        {
            base.Shutdown();
        }
    }
}
