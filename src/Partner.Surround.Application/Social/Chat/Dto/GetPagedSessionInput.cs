﻿using Partner.Surround.CommonDto;

namespace Partner.Surround.Social.Chat.Dto
{
    /// <summary>
    /// 分页获取会话Dto
    /// </summary>
    public class GetPagedSessionInput : PagedInputDto
    {
    }
}
