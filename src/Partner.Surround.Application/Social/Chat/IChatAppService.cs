﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Partner.Surround.Social.Chat.Dto;

namespace Partner.Surround.Social.Chat
{
    public interface IChatAppService : IApplicationService
    {
        Task<ListResultDto<ChatUserDto>> GetAllChatUser();

        Task<ListResultDto<ChatMessageDto>> GetUserChatMessages(GetUserChatMessagesInput input);

        Task<int> UnreadMessageCount();

        Task MarkAllUnreadMessagesOfUserAsRead(MarkAllUnreadMessagesOfUserAsReadInput input);
    }
}
