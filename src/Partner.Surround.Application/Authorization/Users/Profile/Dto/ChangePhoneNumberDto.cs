﻿using Abp.Authorization.Users;
using System.ComponentModel.DataAnnotations;

namespace Partner.Surround.Authorization.Users.Profile.Dto
{
    public class ChangePhoneNumberDto
    {
        [StringLength(AbpUserBase.MaxPhoneNumberLength)]
        public string NewPhoneNumber { get; set; }
    }
}
