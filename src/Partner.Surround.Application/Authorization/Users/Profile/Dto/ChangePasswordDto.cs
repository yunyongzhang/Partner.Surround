﻿using System.ComponentModel.DataAnnotations;

namespace Partner.Surround.Authorization.Users.Profile.Dto
{
    public class ChangePasswordDto
    {
        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}
