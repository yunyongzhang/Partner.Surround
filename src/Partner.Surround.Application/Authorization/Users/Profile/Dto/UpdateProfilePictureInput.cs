namespace Partner.Surround.Authorization.Users.Profile.Dto
{
    public class UpdateProfilePictureInput
    {
        public byte[] ImageBytes { get; set; }
    }
}