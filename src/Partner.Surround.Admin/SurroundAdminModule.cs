﻿using System.IO;
using Abp.AspNetCore;
using Abp.AspNetCore.SignalR;
using Abp.Configuration.Startup;
using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;
using Partner.Surround.Admin.Configuration;
using Partner.Surround.Admin.Views;
using Partner.Surround.EntityFrameworkCore;
using Partner.Surround.Gateway;

namespace Partner.Surround.Admin
{
    [DependsOn(
        typeof(SurroundApplicationModule),
        typeof(SurroundEntityFrameworkModule),
        typeof(SurroundGatewayModule),
        typeof(AbpAspNetCoreModule),
        typeof(AbpAspNetCoreSignalRModule),
        typeof(AbpHangfireAspNetCoreModule)
        )]
    public class SurroundAdminModule : AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public SurroundAdminModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(SurroundCoreConsts.ConnectionStringName);

            // Use database for language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            // 显示所有错误信息到客户端
            Configuration.Modules.AbpWebCommon().SendAllExceptionsToClients = false;

            Configuration.Navigation.Providers.Add<SurroundNavigationProvider>();

            Configuration.BackgroundJobs.UseHangfire();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SurroundAdminModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            SetAppFolders();
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(SurroundAdminModule).Assembly);
        }

        public override void Shutdown()
        {
            base.Shutdown();
        }

        private void SetAppFolders()
        {
            var appFolders = IocManager.Resolve<AppFolders>();

            appFolders.WebLogsFolder = Path.Combine(_env.ContentRootPath, $"App_Data{Path.DirectorySeparatorChar}Logs");
        }
    }
}
