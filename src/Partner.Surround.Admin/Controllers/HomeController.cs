﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;

namespace Partner.Surround.Admin.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : SurroundControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChatPanel()
        {
            return View();
        }
    }
}
