﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;

namespace Partner.Surround.Admin.Controllers
{
    [AbpMvcAuthorize]
    public class WorkSpaceController : SurroundControllerBase
    {
        public ActionResult TenantConsole()
        {
            return View();
        }

        public ActionResult HostConsole()
        {
            return View();
        }
    }
}
