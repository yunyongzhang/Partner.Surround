﻿using Abp.AutoMapper;
using Partner.Surround.Authorization.Roles.Dto;
using Partner.Surround.Admin.Models.Common;

namespace Partner.Surround.Admin.Models.Roles
{
    /// <summary>
    /// 角色分页模型
    /// </summary>
    [AutoMapTo(typeof(GetPagedRoleInput))]
    public class GetPagedRoleViewModel : PagedViewModel
    {
        public string Name { get; set; }
    }
}
