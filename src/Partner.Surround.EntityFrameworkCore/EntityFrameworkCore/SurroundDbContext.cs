﻿using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Partner.Surround.Authorization.Roles;
using Partner.Surround.Authorization.Users;
using Partner.Surround.MultiTenancy;

namespace Partner.Surround.EntityFrameworkCore
{
    public class SurroundDbContext : AbpZeroDbContext<Tenant, Role, User, SurroundDbContext>
    {
        public virtual DbSet<BinaryObjects.BinaryObject> BinaryObject { get; set; }
        public virtual DbSet<Social.Chat.ChatMessage> ChatMessage { get; set; }
        public virtual DbSet<Social.Friendships.Friendship> Friendship { get; set; }
        public virtual DbSet<Resource.DataDictionaries.DataDictionaryItem> DataDictionaryItem { get; set; }
        public virtual DbSet<TaskCenter.DailyTasks.DailyTask> DailyTask { get; set; }

        public SurroundDbContext(DbContextOptions<SurroundDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region DailyTask
            modelBuilder.Entity<TaskCenter.DailyTasks.DailyTask>()
                .OwnsOne(a => a.TaskState, vo =>
                {
                    vo.Property(p => p.Id).HasColumnName("TaskStateType");
                    vo.Ignore(p => p.Name);
                });

            modelBuilder.Entity<TaskCenter.DailyTasks.DailyTask>()
                .OwnsOne(a => a.DateRange, vo =>
                {
                    vo.Property(p => p.StartTime)
                        .HasColumnName("StartTime")
                        .HasColumnType("datetime(6)");

                    vo.Property(p => p.EndTime)
                        .HasColumnName("EndTime")
                        .HasColumnType("datetime(6)");
                });
            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}
