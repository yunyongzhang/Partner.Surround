using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Partner.Surround.EntityFrameworkCore
{
    public static class SurroundDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<SurroundDbContext> builder, string connectionString)
        {
            builder.UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<SurroundDbContext> builder, DbConnection connection)
        {
            builder.UseMySql(connection);
        }
    }
}
