﻿using Abp.Dependency;
using System.Threading.Tasks;

namespace Partner.Surround.TaskCenter.AntiCorruption
{
    public interface IOrganizationService : ITransientDependency
    {
        Task GetOrganizationListAsync();
    }
}
