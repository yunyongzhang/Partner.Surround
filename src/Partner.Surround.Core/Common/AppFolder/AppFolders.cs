﻿using Abp.Dependency;

namespace Partner.Surround
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string WebLogsFolder { get; set; }
    }
}
