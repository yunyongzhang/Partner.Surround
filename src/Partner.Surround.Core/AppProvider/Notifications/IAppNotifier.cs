﻿using System.Threading.Tasks;

namespace Partner.Surround.Notifications
{
    /// <summary>
    /// 消息通知发布接口
    /// </summary>
    public interface IAppNotifier
    {
        Task NewDailyTaskAsync();
    }
}
