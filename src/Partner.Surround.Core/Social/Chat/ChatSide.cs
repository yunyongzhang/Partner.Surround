﻿namespace Partner.Surround.Social.Chat
{
    public enum ChatSide
    {
        Sender = 1,

        Receiver = 2
    }
}
