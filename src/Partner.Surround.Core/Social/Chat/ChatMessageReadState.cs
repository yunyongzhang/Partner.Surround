﻿namespace Partner.Surround.Social.Chat
{
    public enum ChatMessageReadState
    {
        Unread = 1,

        Read = 2
    }
}
