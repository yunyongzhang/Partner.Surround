﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Partner.Surround.EntityFrameworkCore;

namespace Partner.Surround.Admin.Tests
{
    [DependsOn(
        typeof(SurroundAdminModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class SurroundWebTestModule : AbpModule
    {
        public SurroundWebTestModule(SurroundEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SurroundWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(SurroundAdminModule).Assembly);
        }
    }
}