﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Partner.Surround.Storage
{
    public class SurroundStorageModule : AbpModule
    {
        public override void Initialize()
        {
            var thisAssembly = typeof(SurroundStorageModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);
        }
    }
}
