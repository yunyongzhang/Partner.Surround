﻿using Aliyun.OSS;

namespace Partner.Surround.Storage.Aliyun
{
    public class AliyunClientFactory
    {
        public static OssClient Create(AliyunConfig aliyunConfig)
        {
            var ossClient = new OssClient(aliyunConfig.Endpoint, aliyunConfig.AccessKeyId, aliyunConfig.AccessKeySecret);

            return ossClient;
        }
    }
}
